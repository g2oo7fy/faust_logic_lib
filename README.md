# faust_logic_lib

This repository arose during my preperation to a test at univerity
on hardware design.
For some of the tasks there, I though it would be helpful
to model the circuits in a  visual programming environment, like Pure Data.
I even found an implementation, that did quite what I was looking for:

https://github.com/reduzent/pd-mrpeach/tree/master/cmos

The downside here is, that it is implemeted to process controll rate,
which is ok for modelling. But as soon as you would want to do fancier stuff,
the limitation becomes striking.

So I found it a good opportunity to experiment with faust.
As it turned out, it was great fun and brough me through my test quite well ;-)

By knowing this, you might suspect already, that
a basic knowlege of logic circuits is of help,
when you are trying to play around with this.

# Dependencies

The library itself depends only on faust.

For the examples to work best, install the following software packages:

* Pure Data
* faust2pd

Of course, if you'll like to compile it to other architectures,
you migth have other dependencies, too.

You can make usage of the library, by simply adding this code
at the beginning of your .dsp file:

```
import("logic.lib");
```

Most of the library has been implemented as example DSPs already.
The files are very simple, as they just include the line above
plus the obliged call for process and the faust definitions name
as given in the .lib file.

The repository comes with some Makefiles for the diverse platforms in question.
I took them from a former release of faust, but I don't remember exactly which it was.
Please ask, if you want to compile it to architectures not yet provided.

# Usage with Pure Data

## Compilation

Create the Pure Data externals like this:

```bash
make puredata
```

This will create a directory called "puredatadir", where the externals
and an application patch for each of the DSPs is created.
Start Pure Data by including this folder in the search path:

```bash
pd -path ./puredatadir
```

Once Pure Data is running, you can browse to the examples directory
enclosed in the repository.
There you will find some examples for the application of the library in PureData.

By default all these patches include an initialisation subpatch
and a clock, even when it is not used.

The more advanced patches, like TMasterSlave_xample.pd have their own documentation in pd.
For the others, it is recommended to read the comments in the library
and to try to figure out, what the logic circuit with the same (or a similar) name
is meant to do. 
